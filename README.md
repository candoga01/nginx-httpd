# Nginx Http Server
Source: [nginx.org](http://nginx.org)

Alpine based Image for running Nginx HTTP Server as container.

## Desired scope
- base image for static web clients
- base image for proxy chains
- immutable

## Features
- automatically creates selfsigned SSL certificate (ecdsa) on startup
- documentroot located at /var/www
- default listens on port 8443 supporting https and http2
- ssl hardened
- access logs using proxy headers (x-forwarded-for)
- runs under any user-id
- publish server-status at :8000/
- publish healthcheck at :8000/healthz
- timezone support for logs (env TZ=Europe/Berlin)
- modules included: brotli
- configuration files splitted
- supports read-only deployment

## Usage
- can be used as base image
- uses env vars for dynamic runtime properties
- pre-compress.sh script for creating pre-compressed content during target build (gz,br)

## ENV vars

| VAR                      | Default     | expect nginx property    |
| ------------------------ |-------------| -------------------------|
| NGINX_WORKER_PROCESSES   | 1           | worker_processes         |
| NGINX_WORKER_CONNECTIONS | 1024        | worker_connections       |
| NGINX_RLIMIT_NOFILE      | 2048        | worker_rlimit_nofile     |
| NGINX_ERROR_LOG          | info        | error_log level          |
| NGINX_SENDFILE_MAX_CHUNK | 64k         | sendfile_max_chunk       |
| NGINX_KEEPALIVE_TIMEOUT  | 65s         | keepalive_timeout        |
| NGINX_OFC_MAX            | 1000        | open_file_cache max      |
| NGINX_OFC_INACTIVE       | 20s         | open_file_cache inactive |
| NGINX_OFC_VALID          | 30s         | open_file_cache_valid    |


## Tips: force updated timestamp of files BEFORE copying

```Shell
# run this command inside your document root
find . -type f -exec sh -c 'touch -c ${1%};' sh {} \;
```