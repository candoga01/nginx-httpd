#!/bin/sh
set -e

if [ "$DEBUG" ]; then
   set -ux
fi

# functions
docker_pull() {
    docker pull "$1" | grep -e 'Pulling from' -e Digest -e Status -e Error;
}

## load env vars
source "$(pwd)"/build/build.args

IMAGE_NAME=${IMAGE_NAME:-nginx-httpd}
IMAGE_TAG=${IMAGE_TAG:-dev}

## base images
for BASE in $(grep "FROM" build/Dockerfile | cut -d" " -f2 | sort | uniq)
do
   eval docker_pull "$BASE"
done

## model build-args property
BUILD_ARGS=$(while IFS= read -r line; do printf "%s" "--build-arg $line "; done < build/build.args)

## build image
if [ -z "$DEBUG" ]; then
   set -ux
fi

docker build --no-cache --force-rm $BUILD_ARGS -t "$IMAGE_NAME":"$IMAGE_TAG" build/
