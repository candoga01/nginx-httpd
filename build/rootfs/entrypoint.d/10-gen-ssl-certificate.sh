#!/bin/sh
set -e

BASENAME=$( basename "$0" )

echo "[$(date -R)] [$BASENAME] Create selfsigned certificate"
mkdir -p /tmp/nginx
RANDFILE="/tmp/nginx/tls.rnd"
export RANDFILE

SSL_SAN="DNS:microservice,DNS:localhost,DNS:$(hostname -s),IP:$(ip route get 1 | awk '{print $NF;exit}')"
export SSL_SAN

openssl genpkey -genparam -algorithm EC -pkeyopt ec_paramgen_curve:P-256 -out /tmp/nginx/tls.param
openssl req -x509 -nodes -days 3650 \
        -newkey ec:/tmp/nginx/tls.param \
        -subj "/O=Acme Co/CN=Microservice Fake Certificate" \
        -addext "subjectAltName=${SSL_SAN}" \
        -keyout /tmp/nginx/tls.key \
        -out /tmp/nginx/tls.pem \
        > /dev/null 2>&1
