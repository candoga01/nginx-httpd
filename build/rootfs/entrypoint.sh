#!/bin/sh
set -e

# vars required for nginx
NGINX_WORKER_PROCESSES=${NGINX_WORKER_PROCESSES:-1}
export NGINX_WORKER_PROCESSES

NGINX_WORKER_CONNECTIONS=${NGINX_WORKER_CONNECTIONS:-1024}
export NGINX_WORKER_CONNECTIONS

NGINX_RLIMIT_NOFILE=${NGINX_RLIMIT_NOFILE:-2048}
export NGINX_RLIMIT_NOFILE

NGINX_ERROR_LOG=${NGINX_ERROR_LOG:-info}
export NGINX_ERROR_LOG

NGINX_SENDFILE_MAX_CHUNK=${NGINX_SENDFILE_MAX_CHUNK:-64k}
export NGINX_SENDFILE_MAX_CHUNK

NGINX_KEEPALIVE_TIMEOUT=${NGINX_KEEPALIVE_TIMEOUT:-65s}
export NGINX_KEEPALIVE_TIMEOUT

NGINX_OFC_MAX=${NGINX_OFC_MAX:-1000}
export NGINX_OFC_MAX

NGINX_OFC_INACTIVE=${NGINX_OFC_INACTIVE:-20s}
export NGINX_OFC_INACTIVE

NGINX_OFC_VALID=${NGINX_OFC_VALID:-30s}
export NGINX_OFC_VALID

echo "[$(date -R)] [ENTRYPOINT] startup properties:"

DEFINED_VARS=$(env | grep NGINX)
for ITEM in $DEFINED_VARS
do
   echo "$ITEM"
done

# hook scripts
if find "/entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -name '*.sh' -print -quit 2>/dev/null | read -r; then
   HOOK_EXIST=true
fi

if [ "$HOOK_EXIST" = "true" ]; then
   echo "[$(date -R)] [ENTRYPOINT] Found hooks"

   find "/entrypoint.d/" -follow -type f -name '*.sh' -print | sort -n | while read -r hook; do
        echo "[$(date -R)] [ENTRYPOINT] Start hook: $hook"
        /bin/sh "$hook"
        done
fi

# go ahead
if [ "$#" -ne 0 ]; then
   exec "$@"
fi