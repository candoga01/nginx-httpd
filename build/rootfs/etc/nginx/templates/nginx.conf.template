daemon off;

worker_processes  ${NGINX_WORKER_PROCESSES};
worker_rlimit_nofile ${NGINX_RLIMIT_NOFILE};

# Includes files with directives to load dynamic modules.
include /etc/nginx/modules/*.conf;

pid       /tmp/nginx/nginx.pid;
lock_file /tmp/nginx/nginx.lock;

pcre_jit on;

error_log /dev/stderr ${NGINX_ERROR_LOG};

events {
      worker_connections ${NGINX_WORKER_CONNECTIONS};
}

http {
    charset       utf-8;
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    client_body_temp_path /tmp/nginx/client_body_temp;
    proxy_temp_path       /tmp/nginx/proxy_temp;
    fastcgi_temp_path     /tmp/nginx/fastcgi_temp;
    uwsgi_temp_path       /tmp/nginx/uwsgi_temp;
    scgi_temp_path        /tmp/nginx/scgi_temp;

    aio                threads;
    aio_write          on;

    sendfile           on;
    sendfile_max_chunk ${NGINX_SENDFILE_MAX_CHUNK};
    tcp_nopush         on;
    keepalive_timeout  ${NGINX_KEEPALIVE_TIMEOUT};

    server_tokens      off;

    open_file_cache max=${NGINX_OFC_MAX} inactive=${NGINX_OFC_INACTIVE};
    open_file_cache_valid ${NGINX_OFC_VALID};
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    set_real_ip_from 0.0.0.0/32;
    real_ip_header X-Forwarded-For;
    real_ip_recursive on;

    include /etc/nginx/conf.d/logging.conf;
    include /etc/nginx/conf.d/expires.conf;
    include /etc/nginx/conf.d/compression.conf;
    include /etc/nginx/conf.d/headers.conf;
    include /etc/nginx/conf.d/status.conf;
    include /etc/nginx/conf.d/ssl.conf;
    include /etc/nginx/conf.d/default.conf;
   
}
